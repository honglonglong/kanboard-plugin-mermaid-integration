# Mermaid Integration

**Plugin Author:** _[honglonglong](https://gitlab.com/honglonglong)_

# How to use

In kanboard mark down boxes, write code piece with language _mermaid_.
e.g.
````markdown
```mermaid
graph TD;
    A-->B;
    A-->C;
    B-->D;
    C-->D;
```
````

# Implementation

It's very simple.

Because mermaid has provided a wonderful javascript api to generate chart for ```<div>``` with class ```.mermaid```,
and Kanboard wraps mermaid markdown with ```<code class="language-mermaid">```, so the plugin is just to load mermaid javascript with below configuration:
```javascript
mermaid.init(undefined, $("code.language-mermaid"));
```

# Upgrade mermaid
Go to CDN to download latest version of mermaid.min.js and mermaid.min.js.map, and put into Assets/js folder.


# Install

**Kanboard versions tested on 1.2.20**

## Manually

1) Download the latest versions supplied zip file, it should be named `kanboard-plugin-mermaid-integration-x.xx.x.zip`

2) Unzip to the plugins folder.
  - your folder structure should look like the following:
```
plugins
└── MermaidIntegration            <= Plugin name
    ├── Assets    
    ├── LICENSE
    ├── Plugin.php   
    └── README.md
```

3) Restart your server

# Future plans
- To add a configuration panel for mermaid configurations.
- To support mermaid when preview

_This readme document template is copied from [MarkdownPlus](https://github.com/creecros/MarkdownPlus)_
