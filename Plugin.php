<?php

namespace Kanboard\Plugin\MermaidIntegration;


use Kanboard\Core\Plugin\Base;


class Plugin extends Base

{
	public function initialize()
	{
		//CSS
        $this->hook->on('template:layout:css', array('template' => 'plugins/MermaidIntegration/Assets/css/mermaid-integration.css'));
        
        //JS
        $this->hook->on('template:layout:js', array('template' => 'plugins/MermaidIntegration/Assets/js/mermaid.min.js'));
        $this->hook->on('template:layout:js', array('template' => 'plugins/MermaidIntegration/Assets/js/start-mermaid.js'));
        
	}
	
	public function getPluginName()	
	{ 		 
		return 'Mermaid Integration'; 
	}

	public function getPluginAuthor() 
	{ 	 
		return 'honglonglong'; 
	}

	public function getPluginVersion() 
	{ 	 
		return '1.0.0'; 
	}

	public function getPluginDescription() 
	{ 
		return 'To enable mermaid flow chart support for kanboard'; 
	}

	public function getPluginHomepage() 
	{ 	 
		return 'https://gitlab.com/honglonglong/kanboard-plugin-mermaid-integration'; 
	}
}
