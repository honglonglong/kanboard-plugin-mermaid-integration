jQuery(document).ready(function () {
    mermaid.init(undefined, $("code.language-mermaid"));
});
const onChangeElement = (qSelector, cb)=>{
    const targetNode = document.querySelector(qSelector);
    if(targetNode){
       const config = { attributes: true, childList: true, subtree: true };
       const callback = function(mutationsList, observer) {
           cb(document.querySelector(qSelector))
       };
       const observer = new MutationObserver(callback);
       observer.observe(targetNode, config);
    }else {
       console.error("onChangeElement: Invalid Selector")
    }
   }

KB.on('modal.afterRender', function () {
    onChangeElement('.text-editor-preview-area', function(jqueryElement){
        mermaid.init(undefined, $("code.language-mermaid"));
    });
});